#! /usr/bin/env python
import asyncio
import csv
import datetime
import os
import sys
from typing import Dict, Iterable, TypeVar

from aiohttp import ClientSession
from dotenv import load_dotenv
from returns.curry import curry, partial
from returns.future import future_safe
from returns.io import IO, IOSuccess, IOFailure
from returns.pipeline import flow, is_successful
from returns.pointfree import bind_result, map_, bind
from returns.primitives.hkt import kinded, Kind1
from returns.result import safe
from returns.unsafe import unsafe_perform_io
from toolz import first
from toolz.curried import get, drop

T = TypeVar('T', bound=Iterable)


async def main() -> None:
    load_dotenv()
    async with starling_session() as session:
        entries = await flow(account_uuid(session),
                             map_(resource),
                             bind(lambda url: statement(session, url)),
                             map_(lambda x: x.split('\n')),
                             map_(drop(1)),
                             map_(partial(filter, lambda x: len(x) > 0)),
                             map_(split),
                             map_(partial(map, get([0, 1, 4, 5]))),
                             map_(partial(map, partial(map, lambda x: x.strip()))),
                             map_(partial(map, ','.join)),
                             map_('\n'.join))
        match entries:
            case IOSuccess(value):
                print(unsafe_perform_io(value))
            case IOFailure(value):
                sys.stderr.write(f"Failed: '{unsafe_perform_io(value)}'.")



def resource(account_uid: str) -> str:
    now = datetime.datetime.now()
    duration = datetime.timedelta(days=31)
    then = (now - duration)
    return f"/api/v2/accounts/{account_uid}/statement/" \
           f"downloadForDateRange?" \
           f"start={then.strftime('%Y-%m-%d')}&" \
           f"end={now.strftime('%Y-%m-%d')}"


@kinded
def split(xs: Kind1[T, str]) -> Kind1[T, Kind1[T, str]]:
    # return [row for row in csv.reader(xs)]
    return list(csv.reader(xs))


def account_uuid(session: ClientSession):
    return flow(account(session),
                bind_result(safely_get('accounts')),
                bind_result(safely_first),
                bind_result(safely_get('accountUid')))


@curry
@safe
def safely_get(field: str, dictionary: Dict):
    return get(field, dictionary)


@safe
def safely_first(iterable: Kind1[T, str]) -> str:
    return first(iterable)


@future_safe
async def account(session: ClientSession) -> Dict:
    wtf = await session.get("/api/v2/accounts")
    return await wtf.json()


@future_safe
@curry
async def statement(session: ClientSession, resource_path: str):
    response = await session.get(resource_path, headers={'Accept': 'text/csv'})
    return await response.text()


def starling_session() -> ClientSession:
    headers = {"Authorization": f"Bearer {os.environ['STARLING_ACCOUNT_ACCESS_TOKEN']}"}
    return ClientSession("https://api.starlingbank.com", headers=headers)


if __name__ == '__main__':
    asyncio.run(main())
